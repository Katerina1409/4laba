package com.company.departments;

import com.company.clients.BaseVisitor;
import com.company.goods.BaseGoods;
import com.company.interfaces.IDepartment;
import com.company.interfaces.IEmployee;
import com.company.interfaces.IGood;
import com.company.service.BaseEmployee;

import java.util.ArrayList;

public abstract class BaseDepartment implements IDepartment {
    private String name;
    ArrayList <IGood> goodsList = new ArrayList<>();
    ArrayList <IEmployee> employEesList = new ArrayList<>();

    public BaseDepartment(String name, ArrayList<IGood> goodsList, ArrayList<IEmployee> employEesList) {
        this.name = name;
        this.goodsList = goodsList;
        this.employEesList = employEesList;
    }

    public BaseDepartment() { }

    public String getName() {
        return name;
    }

    public ArrayList<IGood> getGoodsList() {
        return goodsList;
    }

    public ArrayList<IEmployee> getEmployEesList() { return employEesList; }

    public void addEmployee(BaseEmployee baseEmployee){
        employEesList.add(baseEmployee);
    }

    public void addGood(BaseGoods baseGoods) {
        goodsList.add(baseGoods);
    }
}




