package com.company.interfaces;

import com.company.goods.BaseGoods;
import com.company.service.BaseEmployee;

import java.util.ArrayList;

public interface IDepartment {
    String getName();
    ArrayList<IGood> getGoodsList();
    ArrayList<IEmployee> getEmployEesList();
    void addEmployee(BaseEmployee baseEmployee);
    void addGood(BaseGoods baseGoods);
}



