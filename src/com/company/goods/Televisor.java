package com.company.goods;

import com.company.interfaces.IDepartment;

public class Televisor extends ElectronicDevice{

    private String model;

    public Televisor(IDepartment department, String name, boolean hasGuarantee, String price, String company, String model) {
        super(department, name, hasGuarantee, price, company);
        this.model = model;
    }

    public Televisor() {
        super();
    }

    public Televisor(String name) {
        super(name);
    }

    public void on(){ }

    public void off(){ }

    public void selectChannel(String name){ }

    public void selectChannel(int channel){ }
}


