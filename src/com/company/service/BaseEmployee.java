package com.company.service;

import com.company.departments.BaseDepartment;
import com.company.interfaces.IDepartment;
import com.company.interfaces.IEmployee;

public abstract class BaseEmployee implements IEmployee {

    private String name;
    public boolean free;
    private IDepartment department;

    public BaseEmployee(String name, boolean free, IDepartment department) {
        this.name = name;
        this.free = free;
        this.department = department;
    }

    public BaseEmployee(boolean free) {
        this.free = free;
    }

    public String getName() {
        return name;
    }

    public IDepartment getDepartment() {
        return department;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }
}

